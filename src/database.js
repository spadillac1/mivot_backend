import mongoose from 'mongoose';
import { initializeFirebase } from './helpers/firebase';
import {Database as Blockchain } from '@southbanksoftware/provendb-node-driver';

export const startMongoose = () => {
  const properties = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  };

  mongoose.connect(
    'mongodb://spadillac1@mivot.provendb.io/mivot?ssl=true',
    properties
  );

  mongoose.connection.once('open', () => {
    console.log('Mongo DB is connected');
  });
}

export const initializeDatabaseSystem = () => {
  //startMongoose();
  initializeFirebase();
  console.log('database was initialized');
};
