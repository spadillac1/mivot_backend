import express from 'express';
import cors from 'cors';

import votations from './routes/votations';
import voters from './routes/voters';
import tokens from './routes/tokens';


const app = express();

app.set('port', process.env.PORT || 5500);
app.use(cors());
app.use(express.json());


app.use('/api/votations', votations);
app.use('/api/voters', voters);
app.use('/api/tokens', tokens);

export { app };
