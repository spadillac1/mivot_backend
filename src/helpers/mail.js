import * as nodemailer from 'nodemailer';
import QR from 'qrcode';
import { generateNewToken } from './token';
import {
  getVotersEmail,
  createVotation,
  deleteVotation,
  getCurrentVotationsId,
  addToken,
} from './firebase';

let link = 'https://mivot-votation-client.vercel.app';

var cuenta = {
  service: 'gmail',
  secure: false,
  auth: {
    user: 'reconecta32@gmail.com',
    pass: '31reconecta',
  },
};

const createTokensCollection = async () => {
  let currenVotationId = await getCurrentVotationsId();
  await deleteVotation(currenVotationId);
  await createVotation();
  return await getCurrentVotationsId();
};

export const sendEmails = async () => {
  const currentVotationId = await createTokensCollection();
  const userData = await getVotersEmail();
  let transporter = nodemailer.createTransport(cuenta);

  for (var i = 0; i < userData.length; i++) {
    let token = generateNewToken(i);
    addToken(currentVotationId, { userId: userData[i].userId, value: token });

    const img = await QR.toDataURL(`${link}/${token}`);

    var destino = {
      to: userData[i].email,
      subject: 'token de votacion',
      attachDataUrls: true,
      html: `Envio de QR para votacion <br> <img src ="  ${img}">`,
    };

    transporter.sendMail(destino);
  }
};
