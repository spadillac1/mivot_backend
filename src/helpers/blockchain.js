import SHA256 from 'crypto-js/sha256';

export let blockchain;

export const initializeBlockchain = () => {
  blockchain = new Blockchain();
  console.log(blockchain)
};

class Block {
  constructor(index, data, previousHash = '') {
    this.index = index;
    this.timestamp = new Date().toUTCString();
    this.data = data;
    this.previousHash = previousHash;
    this.hash = SHA256(
      this.timestamp + this.previousHash + this.data
    ).toString();
  }
}

class Blockchain {
  constructor() {
    this.chain = [new Block(0, 'genesis')];
  }

  getLastBlock() {
    return this.chain[this.chain.length - 1];
  }

  addBlock(data) {
    const lastBlock = this.getLastBlock();
    this.chain.push(new Block(lastBlock.index + 1, data, lastBlock.hash));
  }

  getDataValues() {
    return this.chain.map((block) => block.data);
  }
}
