import jwt from 'jsonwebtoken';

export const generateNewToken = (x) => {
  let token = jwt.sign({ id: x }, 'secreto', { expiresIn: '1h' });

  return token;
};

export const validateToken = (token) => {
  if (!token) {
    return false;
  }

  jwt.verify(token, 'secreto', (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: 'error',
      });
    } else {
      return true;
    }
  });
};

export const deprecateToken = (token) => {};

