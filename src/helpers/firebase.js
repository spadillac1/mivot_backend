import firebase from 'firebase';
import 'firebase/firestore';

import { collections } from './enviroment';

const firebaseConfig = {
  apiKey: 'AIzaSyAmwd6U9IIy5Qb7OenQ_LAFjJJXy3n82Pc',
  authDomain: 'mivot-306fe.firebaseapp.com',
  databaseURL: 'https://mivot-306fe.firebaseio.com',
  projectId: 'mivot-306fe',
  storageBucket: 'mivot-306fe.appspot.com',
  messagingSenderId: '585013058583',
  appId: '1:585013058583:web:890dc90deb6a38a2db9052',
};

let db = null;

export const initializeFirebase = () => {
  firebase.initializeApp(firebaseConfig);
  db = firebase.firestore();
};

export const deleteVotation = (id) => {
  return db.collection(collections.votations).doc(id).delete();
};

export const createVotation = () => {
  return db.collection(collections.votations).add({ tokens: [] });
};

export const getCurrentVotationsId = () => {
  return db
    .collection(collections.votations)
    .get()
    .then((r) => r.docs && r.docs[0].id);
};

export const addToken = (id, { value, userId }) => {
  const tokenObject = { value, userId, hasUsed: false };
  return db
    .collection(collections.votations)
    .doc(id)
    .update({
      tokens: firebase.firestore.FieldValue.arrayUnion(tokenObject),
    });
};

export const getVotersFirebase = () => {
  return db
    .collection(collections.voters)
    .get()
    .then((r) => r.docs.map((doc) => ({ _id: doc.id, ...doc.data() })));
};

const getTokens = () => {
  return db
    .collection(collections.votations)
    .get()
    .then((r) => r.docs.map((doc) => doc.data()['tokens'])[0]);
};

export const canUseToken = async (userToken) => {
  const tokens = await getTokens();
  const findedToken = tokens.find((token) => token.value == userToken);
  return findedToken
    ? { valid: !findedToken.hasUsed, userId: findedToken.userId }
    : { valid: false };
};

export const deprecateToken = async (userToken) => {
  const tokens = await getTokens();
  const updatedTokens = tokens.map((token) => {
    token.value == userToken ? (token.hasUsed = true) : null;
    return token;
  });
  console.log(updatedTokens)
  const currentId = await getCurrentVotationsId();
  return db.collection(collections.votations).doc(currentId).update({
    tokens: updatedTokens,
  });
};

export const getVotersEmail = () => {
  return db
    .collection(collections.voters)
    .get()
    .then((r) =>
      r.docs.map((doc) => ({ email: doc.data()['email'], userId: doc.id }))
    );
};

export const updateVoter = async (id) => {
  return db.collection(collections.voters).doc(id).update({ hasVoted: true });
};

export const hasVoted = (id) => {
  return db
    .collection(collections.voters)
    .doc(id)
    .get()
    .then((doc) => doc.data()['hasVoted']);
};
