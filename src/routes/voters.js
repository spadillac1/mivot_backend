import { Router } from 'express';

import {
  getVoters,
  postVote,
} from '../controllers/voters.controller';

const router = Router();

router.route('/').get(getVoters);
router.route('/:token').post(postVote);

export default router;
