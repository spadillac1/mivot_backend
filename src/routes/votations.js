import { Router } from 'express';

import {
  getVotationResults,
  postInitVotations,
} from '../controllers/votations.controller';

const router = Router();

router.route('/').get(getVotationResults).post(postInitVotations);

export default router;
