import { Router } from 'express';

import { postTokenValidation } from '../controllers/tokens.controller';

const router = Router();

router.route('/:token').post(postTokenValidation);

export default router;
