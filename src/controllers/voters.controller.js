import {
  getVotersFirebase,
  updateVoter,
  deprecateToken,
} from '../helpers/firebase';

import { blockchain } from '../helpers/blockchain';

export const getVoters = (req, res) => {
  getVotersFirebase()
    .then((data) => res.send(data))
    .catch((error) => ((res.status = '300'), res.send(error.message)));
};

export const postVote = async (req, res) => {
  const { vote, userId } = req.body;
  const { token } = req.params;
  if (blockchain && vote !== undefined && vote !== null) {
    try {
      blockchain.addBlock(vote);
      await updateVoter(userId);
      await deprecateToken(token);
      res.send(`${vote} + ${userId} was updated`);
    } catch (error) {
      res.status(300);
      res.render({message: error.message});
    }
  } else {
    res.status(300).send('Current blockchain or request error');
  }
};
