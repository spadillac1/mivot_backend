import { canUseToken } from "../helpers/firebase";
export const postTokenValidation = (req, res) => {
  const { token } = req.params;
    canUseToken(token).then((result) => res.send(result)).catch(() => {
      res.status = 400;
      res.send(result);
    })
};
