import { sendEmails } from "../helpers/mail";
import { initializeBlockchain } from '../helpers/blockchain';

export const getVotationResults = (req, res) => {
  console.log('getVotationResults');
  res.send('getVotationResults');
};

export const postInitVotations = (req,res) => {
  initializeBlockchain();
  sendEmails().then(() =>{
    res.send('Las votaciones han iniciado');
  })
};
